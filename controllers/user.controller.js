const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const rabbitmq = require('../config/rabbitmq');
const async = require("async");
const redis = require('redis');
const fileUpload = require('express-fileupload');
var events = require('events');
var eventEmitter = new events.EventEmitter();
eventEmitter.addListener('connection', rabbitmq.publish);
const AWS = require('aws-sdk');
const fs = require('fs');
const path = require('path');

var eventListeners = require('events').EventEmitter.listenerCount(eventEmitter, 'connection');

// redis
const client = redis.createClient(6379);
client.on('connect', function () {
    console.log('Redis client connected');
});
// echo redis errors to the console
client.on('error', (err) => {
    console.log("Error " + err)
});

let response = {
    success: false,
    message: "Something bad happended",
    data: {}
};
// Authenticate
var authenticate = function (req, res, next) {
    const username = req.body.username;
    const password = req.body.password;

    async.waterfall([

        function (callback) {
            User.getUserByUsername(username, (err, user) => {
                if (err) throw err;
                if (!user) {
                    return res.json({success: false, msg: 'User not found'});
                }

                User.comparePassword(password, user.password, (err, isMatch) => {

                    if (err) throw err;
                    if (isMatch) {
                        const token = jwt.sign({data: user}, config.secret, {
                            expiresIn: 604800 // 1 week
                        });

                        let obj = {
                            token: `Bearer ${token}`,
                            user: {
                                id: user._id,
                                name: user.name,
                                username: user.username,
                                email: user.email
                            }
                        };
                        return callback(null, obj);
                    } else {
                        return callback("Wrong Password");
                        // return res.json({success: false, msg: 'Wrong password'});
                    }
                });
            });

        },
        function (arg1, callback) {
            client.set(`UserToken${arg1.user.id}`, JSON.stringify({
                'UserId': arg1.user.id,
                'Token': arg1.token
            }), redis.print);
            client.get(`UserToken${arg1.user.id}`, function (error, result) {
                if (error) {
                    console.log(error);
                    throw error;
                }
                console.log('GET result ->' + result);
                return callback(null, arg1);
            });
        }

    ], function (err, results) {
        // optional callback
        if (err) {
            return res.status(400).send(response);
        } else {
            response.success = true;
            response.message = "Updated";
            response.data = results;
            return res.status(200).send(response);
        }
    });
};

// Register
var register = function (req, res, next) {

    host = req.hostname;
    async.waterfall([
            function (callback) {
                let newUser = new User({
                    name: req.body.name,
                    email: req.body.email,
                    username: req.body.username,
                    password: req.body.password
                });
                User.addUser(newUser, (err, user) => {
                    if (err) {
                        if (err.code === 11000) {
                            res.json({success: false, message: 'User already registered'});
                            return callback("User already registered");
                        }
                    } else {
                        //  res.json({success: true, message: 'User registered', data: user});
                        return callback(null, newUser);
                    }
                });
            },
            function (newUser, callback) {
                eventEmitter.emit('connection', "", "jobs", {newUser, host});
                return callback(null, newUser);
            }
        ],
        function (err, results) {
            if (err) {
                console.log(err, "Err line 179");
                return res.status(400).send(response);
            } else {
                // console.log(results, "respo");
                response.success = true;
                response.message = "User Registered";
                response.data = results;
                return res.status(200).send(response);
            }
        });
};

// Profile
var profile = function (req, res) {
    res.json({user: req.user});
};

// S3 bucket
AWS.config.update({
    accessKeyId: "AKIAISJYJ2QWODFZPCVQ",
    secretAccessKey: "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",
    "region": "ap-northeast-1"
});

// Upload
var upload = function (req, res) {
    if (Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }

    let sampleFile = req.files.sampleFile;

    var s3 = new AWS.S3();
    var filePath = "./uploads/" + sampleFile.name;
    setTimeout(() => {
        var params = {
            Bucket: 'upload-storage1',
            Body: fs.createReadStream(filePath),
            Key: "uploads/" + Date.now() + "_" + path.basename(filePath)
        };
        s3.upload(params, function (err, data) {
            //handle error
            if (err) {
                console.log("Error", err);
            }

            //success
            if (data) {
                console.log("Uploaded in:", data.Location);
            }
        });
    }, 500);
    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv('./uploads/' + sampleFile.name, function (err) {
        if (err)
            return res.status(500).send(err);

        res.send('File uploaded!');
    });
};


var obj = {
    authenticate, register, profile, upload
};
module.exports = obj;
