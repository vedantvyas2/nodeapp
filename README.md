**Node Application using:**
 - Async - parallel, waterfall, series
 - Redis
 - JWT
 - passport
 - Swagger
 - S3 bucket
 - Express File Upload
 - RabbitMQ

**APIs:**
 - Register  _// To register user details and to send mail for confirmation._
 - Login _// To login into the application_
 - Profile _// To allow authorize users to route to profile page_
 - Upload _// To upload images or any files to s3 bucket and also in uploads folder._
 
 **Database:**
 - MongoDB
