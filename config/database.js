const mongoose = require('mongoose');
const config = require('../config/config');

var db = mongoose.connection;
var init = function () {
    mongoose.connect(config.database, {useMongoClient: true, promiseLibrary: require('bluebird')});
    db.on('connecting', function () {
        console.log('connecting to MongoDB...');
    });

    db.on('error', function (error) {
        console.error('Error in MongoDb connection: ' + error);
        mongoose.disconnect();
    });
    db.on('connected', function () {
        console.log('MongoDB connected!');
    });
    db.once('open', function () {
        console.log('MongoDB connection opened!');
    });
    db.on('reconnected', function () {
        console.log('MongoDB reconnected!');
    });
    db.on('disconnected', function () {
        console.log('MongoDB disconnected!');
        mongoose.connect(config.database, {
            useMongoClient: true,
            promiseLibrary: require('bluebird')
        }, {server: {auto_reconnect: true}});
    });
};

module.exports = {init};
