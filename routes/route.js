const express = require('express');
const router = express.Router();
const passport = require('passport');
const nodemailer = require("nodemailer");
const userController = require('../controllers/user.controller');
const usermailer = require('../config/nodemailer');

/**
 * @swagger
 * /register:
 *    post:
 *      description: This should register new user
 *      requestBody:
 *        description: Optional description in *Markdown*
 *        required: true
 *        consumes:
 *          application/json:
 *        parameters:
 *          - in: body
 *            name: body
 *            description: The user to create.
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                username:
 *                  type: string
 *                email:
 *                  type: string
 *                password:
 *                  type: string
 *              required:
 *                - email
 *                - username
 *                - password
 *            style: form
 *      responses:
 *        '201':
 *          description: Created
 *      components:
 *        securitySchemes:
 *          BasicAuth:
 *            type: http
 *            scheme: basic
 *          security:
 *            - BasicAuth: []
 */
router.post('/register', userController.register);

/**
 * @swagger
 * /authenticate:
 *    post:
 *      description: This should authenticate users
 */
router.post('/authenticate', userController.authenticate);

/**
 * @swagger
 * /profile:
 *    get:
 *      description: This should show user profile
 *      security:
 */
router.get('/profile', passport.authenticate('jwt', {session: false}), userController.profile);


//Verify email
router.get('/verify', usermailer.verify);

/**
 * @swagger
 * /uploads:
 *    post:
 *      description: This should upload user files
 *      requestBody:
 *        description: Optional description in *Markdown*
 *        content:
 *          multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              orderId:
 *                type: integer
 *                userId:
 *                  type: integer
 *                fileName:
 *                  type: string
 *                  format: binary
 *      responses:
 *        '201':
 *          description: Created
 */
router.post('/upload', userController.upload);

module.exports = router;
